package br.com.desafio.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SeleniumUtils {

    public static void higthLine(WebElement locator, boolean arg) {
        try {
            String color = arg ? "outline: 4px solid #00FF00;" : "outline: 4px solid #ff0000;";
            JavascriptExecutor javaScriptExecutor = (JavascriptExecutor) SetUp.instance().getDriver();
            javaScriptExecutor.executeScript("arguments[0].setAttribute('style', arguments[1]);",
                    locator, color);

        } catch (Exception e) {
        }

    }

    public static void setSelectVisableText(WebElement element, String arg) {

        try {
            Select selectElement = new Select(element);
            if (element.isDisplayed() && element.isEnabled()) {
                higthLine(element, true);
                selectElement.selectByVisibleText(arg);
            }

        } catch (Exception e) {
        }

    }


}
