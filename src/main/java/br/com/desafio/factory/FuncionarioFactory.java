package br.com.desafio.factory;

import br.com.desafio.dto.FuncionarioDto;
import br.com.desafio.util.Utils;
import com.github.javafaker.Faker;

import java.util.Random;

public class FuncionarioFactory {

    private static FuncionarioFactory instance;

    public FuncionarioDto getFuncionario() {
        return funcionarioDto;
    }

    private FuncionarioDto funcionarioDto;

    private FuncionarioFactory() {
        funcionarioDto = buildNewFuncionario();
    }

    public static FuncionarioFactory instance() {
        if (instance == null) {
            instance = new FuncionarioFactory();

        }
        return instance;
    }

    public FuncionarioDto buildNewFuncionario() {

        Integer randSexo = new Random().nextInt(3);
        return FuncionarioDto.builder()
                .admissao(Utils.getActualDateTime("dd/MM/YYYY", 0))
                .cargo("ANALISTA DE QUALIDADE")
                .comissao("2.000,00")
                .departamentoId("1")
                .nome(Faker.instance().name().firstName())
                .sexo(randSexo == 0 ? "Indiferente" : randSexo == 1 ? "Feminino" : "Masculino")
                .cpf(Utils.cpfGenarator(true, true))
                .salario("6.500,00")
                .tipoContratacao(new Random().nextBoolean() ? "clt" : "pj")
                .build();

    }

}
