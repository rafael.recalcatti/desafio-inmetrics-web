package br.com.desafio.factory;

import br.com.desafio.dto.UserDto;
import com.github.javafaker.Faker;

public class UserFactory {

    private static UserFactory instance;

    public UserDto getUserDto() {
        return userDto;
    }

    private UserDto userDto;

    private UserFactory() {
        userDto = buildNewUser();
    }

    public static UserFactory instance() {
        if (instance == null) {
            instance = new UserFactory();

        }
        return instance;
    }

    private UserDto buildNewUser() {
        return UserDto.builder()
                .username(Faker.instance().name().username())
                .password(Faker.instance().internet().password())
                .build();
    }
}
