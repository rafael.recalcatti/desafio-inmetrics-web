package br.com.desafio.pages.acesso;

import br.com.desafio.dto.UserDto;
import br.com.desafio.factory.UserFactory;
import br.com.desafio.util.SeleniumUtils;
import br.com.desafio.util.SetUp;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class AcessoPageAction extends AcessoElementMap {

    private WebDriver driver;

    public AcessoPageAction() {
        PageFactory.initElements(SetUp.instance().getDriver(), this);
    }

    public void acessoPagina() {
        driver = SetUp.instance().getDriver();
        driver.navigate().to("http://www.inmrobo.tk/accounts/login/");
    }

    public void inserirDadosDeAcesso(String user, String pass) {
        SeleniumUtils.higthLine(inputUser, true);
        inputUser.sendKeys(user);
        SeleniumUtils.higthLine(inputPass, true);
        inputPass.sendKeys(pass);
    }

    public void acessarSistema() {
        SeleniumUtils.higthLine(btnEntrar, true);
        btnEntrar.click();
    }

    public boolean validarAcesso() {
        boolean status = false;
        try {
            status = tabelaHomePage.isDisplayed();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        SeleniumUtils.higthLine(tabelaHomePage, true);
        SetUp.instance().print();
        return status;
    }

    public boolean validarAcessoInvalido() {
        boolean status = textErrorAccess.isDisplayed();
        SeleniumUtils.higthLine(textErrorAccess, true);
        SetUp.instance().print();
        return status;
    }

    public void clicarCadastroNovoUsuario() {
        SeleniumUtils.higthLine(linkCadastro, true);
        linkCadastro.click();
    }

    public void inserirNovoUsuarioESenha() {

        UserDto userDto = UserFactory.instance().getUserDto();
        SeleniumUtils.higthLine(inputUser, true);
        inputUser.sendKeys(userDto.getUsername());
        SeleniumUtils.higthLine(inputPass, true);
        inputPass.sendKeys(userDto.getPassword());
        SeleniumUtils.higthLine(inputConfirmPass, true);
        inputConfirmPass.sendKeys(userDto.getPassword());
    }

    public void clicarCadastrar() {
        SeleniumUtils.higthLine(btnCadastrarUsuario, true);
        btnCadastrarUsuario.click();
    }

    public boolean validarRedirecionamentoTelaLogin() {
        SetUp.instance().print();
        return SetUp.instance().getDriver().getCurrentUrl().equals("http://www.inmrobo.tk/accounts/login/");
    }
}
