package br.com.desafio.pages.acesso;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AcessoElementMap {

    @FindBy(name = "username")
    protected WebElement inputUser;

    @FindBy(name = "pass")
    protected WebElement inputPass;

    @FindBy(name = "confirmpass")
    protected WebElement inputConfirmPass;

    @FindBy(xpath = "//button[@Class='login100-form-btn']")
    protected WebElement btnEntrar;

    @FindBy(xpath = "//table[@Id='tabela']")
    protected WebElement tabelaHomePage;

    @FindBy(xpath = "//strong[contains(.,'ERRO!')]//..//../div[contains(.,'ou Senha inv')]")
    protected WebElement textErrorAccess;

    @FindBy(xpath = "//div/a[contains(., 'Cadastre-se')]")
    protected WebElement linkCadastro;

    @FindBy(xpath = "//button[contains(., 'Cadastrar')]")
    protected WebElement btnCadastrarUsuario;
}
