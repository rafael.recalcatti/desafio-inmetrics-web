package br.com.desafio.pages.funcionario;

import br.com.desafio.dto.FuncionarioDto;
import br.com.desafio.util.SeleniumUtils;
import br.com.desafio.util.SetUp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class FuncionarioPageAction extends FuncionarioElementMap {

    public FuncionarioPageAction() {
        PageFactory.initElements(SetUp.instance().getDriver(), this);
    }

    public void clicarNovoFuncionario() {
        linkNovoFuncionario.click();
    }

    public void iserirDadosFuncionario(FuncionarioDto funcionarioDto) {

        SeleniumUtils.higthLine(inputCpf, true);
        inputCpf.sendKeys(funcionarioDto.getCpf());

        SeleniumUtils.higthLine(inputNome, true);
        inputNome.sendKeys(funcionarioDto.getNome());

        SeleniumUtils.higthLine(inputCargo, true);
        inputCargo.sendKeys(funcionarioDto.getCargo());

        SeleniumUtils.higthLine(inputSalario, true);
        inputSalario.sendKeys(funcionarioDto.getSalario());

        SeleniumUtils.higthLine(inputDataAdmissao, true);
        inputDataAdmissao.sendKeys(funcionarioDto.getAdmissao());

        SeleniumUtils.setSelectVisableText(comboSexo, funcionarioDto.getSexo());

        switch (funcionarioDto.getTipoContratacao()) {
            case "clt":
                SeleniumUtils.higthLine(checkTipoContratacaoCLT, true);
                checkTipoContratacaoCLT.click();
                break;

            case "pj":
                SeleniumUtils.higthLine(checkTipoContratacaoPJ, true);
                checkTipoContratacaoPJ.click();
                break;
        }

    }

    public void clicarEnviarNovoFuncionario() {
        SeleniumUtils.higthLine(btnEnviar, true);
        btnEnviar.click();
    }

    public boolean validarCadastroNovoFuncionario() {
        boolean status = false;
        try {
            status = textSucessoCadastroFuncionario.isDisplayed();
        } catch (Exception e) {
        }
        SeleniumUtils.higthLine(textSucessoCadastroFuncionario, true);
        SetUp.instance().print();
        return status;
    }

    public void pesquisarFuncionarioPorCpf(String cpf) {
        inputPesquisarFuncionario.sendKeys(cpf);
    }

    public void clicarEditar(String cpf) {
        WebElement element = SetUp.instance().getDriver().findElement(By.xpath("//table[@id='tabela']//td[text()='" + cpf + "']/following-sibling::td//span[@class='fa fa-pencil']"));
        SeleniumUtils.higthLine(element, true);
        element.click();
    }

    public void alaterarInformacaoFuncionario() {
        SeleniumUtils.higthLine(inputNome, true);
        inputNome.clear();
        inputNome.sendKeys("TESTE ALTERACAO");
    }

    public void clicarRemover(String cpf) {
        WebElement element = SetUp.instance().getDriver().findElement(By.xpath("//table[@id='tabela']//td[text()='" + cpf + "']/following-sibling::td//span[@class='fa fa-trash']"));
        SeleniumUtils.higthLine(element, true);
        element.click();
    }

    public boolean validarAlteracaoFuncionario() {
        boolean status = false;
        try {
            status = textSucessoAlteracaoFuncionario.isDisplayed();
        } catch (Exception e) {
        }
        SeleniumUtils.higthLine(textSucessoAlteracaoFuncionario, true);
        SetUp.instance().print();
        return status;
    }

    public boolean validarRemocaoFuncionario() {
        boolean status = false;
        try {
            status = textSucessoRemocaoFuncionario.isDisplayed();
        } catch (Exception e) {
        }
        SeleniumUtils.higthLine(textSucessoRemocaoFuncionario, true);
        SetUp.instance().print();
        return status;
    }

}
