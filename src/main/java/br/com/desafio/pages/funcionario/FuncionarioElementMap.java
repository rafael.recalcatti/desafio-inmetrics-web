package br.com.desafio.pages.funcionario;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FuncionarioElementMap {

    @FindBy(name = "cpf")
    protected WebElement inputCpf;

    @FindBy(name = "nome")
    protected WebElement inputNome;

    @FindBy(name = "cargo")
    protected WebElement inputCargo;

    @FindBy(name = "salario")
    protected WebElement inputSalario;

    @FindBy(name = "admissao")
    protected WebElement inputDataAdmissao;

    @FindBy(xpath = "//label[text()='CLT']/preceding-sibling::input[@Type='radio']")
    protected WebElement checkTipoContratacaoCLT;

    @FindBy(xpath = "//label[text()='PJ']/preceding-sibling::input[@Type='radio']")
    protected WebElement checkTipoContratacaoPJ;

    @FindBy(xpath = "//select[@Name='sexo']")
    protected WebElement comboSexo;

    @FindBy(xpath = "//a[contains(., 'Novo Funcion')]")
    protected WebElement linkNovoFuncionario;

    @FindBy(className = "cadastrar-form-btn")
    protected WebElement btnEnviar;

    @FindBy(xpath = "//strong[contains(.,'SUCESSO!')]//..//../div[contains(.,'Usuário cadastrado com sucesso')]")
    protected WebElement textSucessoCadastroFuncionario;

    @FindBy(xpath = "//strong[contains(.,'SUCESSO!')]//..//../div[contains(.,'Informações atualizadas com sucesso')]")
    protected WebElement textSucessoAlteracaoFuncionario;

    @FindBy(xpath = "//strong[contains(.,'SUCESSO!')]//..//../div[contains(.,'Funcionário removido com sucesso')]")
    protected WebElement textSucessoRemocaoFuncionario;

    @FindBy(xpath = "//label[text() = 'Pesquisar:']/input")
    protected WebElement inputPesquisarFuncionario;


    //table[@id='tabela']//td[text()='021.346.190-07']/following-sibling::td//span[@class='fa fa-trash']
}

