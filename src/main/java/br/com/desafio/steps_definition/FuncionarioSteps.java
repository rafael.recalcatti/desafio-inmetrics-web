package br.com.desafio.steps_definition;

import br.com.desafio.dto.FuncionarioDto;
import br.com.desafio.factory.FuncionarioFactory;
import br.com.desafio.pages.funcionario.FuncionarioPageAction;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.testng.Assert;

public class FuncionarioSteps {

    private FuncionarioPageAction action = new FuncionarioPageAction();
    private FuncionarioDto funcionarioDto = FuncionarioFactory.instance().getFuncionario();

    @Dado("^cliquei em novo funcionario$")
    public void cliqueiEmNovoFuncionario() {
        action.clicarNovoFuncionario();
    }

    @E("^inseri os dados do funcionario$")
    public void inseriOsDadosDoFuncionario() {
        action.iserirDadosFuncionario(funcionarioDto);
    }

    @Quando("^clico em enviar$")
    public void clicoEmEnviar() {
        action.clicarEnviarNovoFuncionario();
    }

    @Entao("^vejo a mensagem de funcionario cadastrado com sucesso$")
    public void vejoAMensagemDeFuncionarioCadastradoComSucesso() {
        Assert.assertTrue(action.validarCadastroNovoFuncionario());
    }

    @Dado("^inseri o cpf do funcionario no campo pesquisa$")
    public void inseriOCpfDoFuncionarioNoCampoPesquisa() {
        action.pesquisarFuncionarioPorCpf(funcionarioDto.getCpf());
    }

    @E("^cliquei em editar$")
    public void cliqueiEmEditar() {
        action.clicarEditar(funcionarioDto.getCpf());
    }

    @E("^alterei o registro do funcionario$")
    public void altereiORegistroDoFuncionario() {
        action.alaterarInformacaoFuncionario();
    }

    @Entao("^vejo a mensagem de funcionario alterado com sucesso$")
    public void vejoAMensagemDeFuncionarioAlteradoComSucesso() {
        Assert.assertTrue(action.validarAlteracaoFuncionario());
    }

    @E("^cliquei em remover$")
    public void cliqueiEmRemover() {
        action.clicarRemover(funcionarioDto.getCpf());
    }

    @Entao("^vejo a mensagem de funcionario removido com sucesso$")
    public void vejoAMensagemDeFuncionarioRemovidoComSucesso() {
        Assert.assertTrue(action.validarRemocaoFuncionario());
    }
}
