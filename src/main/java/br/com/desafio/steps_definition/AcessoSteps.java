package br.com.desafio.steps_definition;


import br.com.desafio.dto.UserDto;
import br.com.desafio.factory.UserFactory;
import br.com.desafio.pages.acesso.AcessoPageAction;
import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.testng.Assert;

public class AcessoSteps {

    private AcessoPageAction action = new AcessoPageAction();

    @Dado("^acessei a pagina de login do sistema$")
    public void acesseiAPaginaDeLoginDoSistema() {
        action.acessoPagina();
    }

    @E("inseri o usuario e senha previamente cadastrados")
    public void inseriOUsuarioESenhaPreviamenteCadastrados() {
        UserDto userDto = UserFactory.instance().getUserDto();
        action.inserirDadosDeAcesso(userDto.getUsername(), userDto.getPassword());
    }

    @Quando("clico em entrar")
    public void clicoEmEntrar() {
        action.acessarSistema();
    }

    @Entao("vejo o sistema aberto")
    public void vejoOSistemaAberto() {
        Assert.assertTrue(action.validarAcesso());
    }

    @E("^cliquei em cadastre-se$")
    public void cliqueiEmCadastreSe() {
        action.clicarCadastroNovoUsuario();
    }

    @E("^inseri o usuario a senha e confirmacao de senha$")
    public void inseriOUsuarioASenhaEConfirmacaoDeSenha() {
        action.inserirNovoUsuarioESenha();
    }

    @Quando("^clico em cadastrar$")
    public void clicoEmCadastrar() {
        action.clicarCadastrar();
    }

    @Entao("^sou redirecionado para a tela de login$")
    public void souRedirecionadoParaATelaDeLogin() {
        Assert.assertTrue(action.validarRedirecionamentoTelaLogin());
    }

    @E("^inseri o usuario e senha nao cadastrados$")
    public void inseriOUsuarioESenhaNaoCadastrados() {
        UserDto userDto = UserFactory.instance().getUserDto();
        action.inserirDadosDeAcesso(userDto.getUsername(), userDto.getPassword());
    }

    @Entao("^vejo a mensagem de usuario ou senha invalidos$")
    public void vejoAMensagemDeUsuarioOuSenhaInvalidos() {
        Assert.assertTrue(action.validarAcessoInvalido());
    }

    @E("^inseri o usuario \"([^\"]*)\" e senha \"([^\"]*)\"$")
    public void inseriOUsuarioESenha(String user, String pass) throws Throwable {
        action.inserirDadosDeAcesso(user, pass);
    }
}
