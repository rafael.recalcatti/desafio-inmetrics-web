package br.com.desafio.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserDto {
    private String username;
    private String password;
}
