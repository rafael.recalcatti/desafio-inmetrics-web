package br.com.desafio.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FuncionarioDto {

    private Integer empregadoId;
    private String nome;
    private String sexo;
    private String departamentoId;
    private String cpf;
    private String cargo;
    private String admissao;
    private String salario;
    private String comissao;
    private String tipoContratacao;
}
