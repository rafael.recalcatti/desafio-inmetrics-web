package br.com.desafio.tescase;

import br.com.desafio.util.SetUp;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.*;

@CucumberOptions(
        features = "classpath:features",
        tags = {"@Funcionario, @Usuario"},
        plugin = {
                "json:target/cucumber-reports/cucumberTestReport.json"},
        glue = "br.com.desafio.steps_definition",
        monochrome = true,
        dryRun = false)

@Listeners(ExtentITestListenerClassAdapter.class)
public class TestRunnerNG {

    private TestNGCucumberRunner testNGCucumberRunner;
    private Scenario cenario;

    @BeforeClass(alwaysRun = true)
    public void setUpClass() throws Exception {
        System.out.print("\n>>>>>>BEFORE CLASS<<<<<<\n");
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() throws Exception {
        System.out.print("\n>>>>>>BEFORE TEST<<<<<<\n");
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void features(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        System.out.print("\n>>>>>>AFTER CLASS<<<<<<\n");
        testNGCucumberRunner.finish();
        SetUp.instance().getDriver().quit();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownMethod() throws Exception {
        System.out.print("\n>>>>>>AFTER METHOD<<<<<<\n");
    }

    @AfterTest(alwaysRun = true)
    public void tearDownTest() throws Exception {
        //SetUp.instance().getDriver().quit();
    }


}
