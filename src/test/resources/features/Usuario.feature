#language: pt
@Usuario
Funcionalidade: Cadastrar Usuario e Acessar Sistema
  Como novo usuario do sistema
  quero me cadastrar no sistema
  para acessar o mesmo e poder utilizar suas funcionalidades

  @LoginErro
  Cenario: Acessar Sistema sem Sucesso
    Dado acessei a pagina de login do sistema
    E inseri o usuario e senha nao cadastrados
    Quando clico em entrar
    Entao vejo a mensagem de usuario ou senha invalidos

  @CadastrarUsuario
  Cenario: Cadastrar Usuario com Sucesso
    Dado acessei a pagina de login do sistema
    E cliquei em cadastre-se
    E inseri o usuario a senha e confirmacao de senha
    Quando clico em cadastrar
    Entao sou redirecionado para a tela de login

  @LoginSucesso
  Cenario: Acessar Sistema com Sucesso
    Dado acessei a pagina de login do sistema
    E inseri o usuario e senha previamente cadastrados
    Quando clico em entrar
    Entao vejo o sistema aberto

