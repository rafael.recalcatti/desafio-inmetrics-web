#language: pt
@Funcionario
Funcionalidade: Cadastrar Editar e Excluir Funcionario
  Como usuario do sistema
  quero cadastrar editar ou excluir um funcionario
  para gravar, editar ou remover suas informações da base de dados

 Contexto: Acessar sistema
    Dado acessei a pagina de login do sistema
    E inseri o usuario "boyce.schumm" e senha "hxv2n1nhdjpdvw"
    Quando clico em entrar

  @CadastroFuncionario
  Cenario: Cadastrar Funcionario com Sucesso
    Dado cliquei em novo funcionario
    E inseri os dados do funcionario
    Quando clico em enviar
    Entao vejo a mensagem de funcionario cadastrado com sucesso

  @EditarFuncionario
  Cenario: Editar Funcionario com Sucesso
    Dado inseri o cpf do funcionario no campo pesquisa
    E cliquei em editar
    E alterei o registro do funcionario
    Quando clico em enviar
    Entao vejo a mensagem de funcionario alterado com sucesso

  @ExcluirFuncionario
  Cenario: Excluir Funcionario com Sucesso
    Dado inseri o cpf do funcionario no campo pesquisa
    E cliquei em remover
    Entao vejo a mensagem de funcionario removido com sucesso